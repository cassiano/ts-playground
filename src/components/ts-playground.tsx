// https://microsoft.github.io/monaco-editor/index.html
// https://github.com/microsoft/monaco-editor
// https://betterprogramming.pub/embedding-a-monaco-editor-inside-react-app-37083cf829e9
// https://stackoverflow.com/questions/23075748/how-to-compile-typescript-code-in-the-browser
// https://microsoft.github.io/monaco-editor/api/interfaces/monaco.editor.IEditorOptions.html

import React, { useCallback, useEffect, useMemo, useState } from 'react'
import {
  copyTextToClipboard,
  debounce,
  formatLogEntry,
  getErrorMessage,
  groupEnum,
  transpileTs,
} from '../util'
import Editor from '@monaco-editor/react'
import lz from 'lz-string'
import { createShortUrlApi } from '../services/api'
import { useRefState } from '../hooks/use-ref-state'
import { CompilerOptions, ModuleKind, ScriptTarget } from 'typescript'

export type ShortUrlType = {
  id: number
  uniqueId: string
  links: {
    visit: string
  }
  visited: number
}

export type EditorOptions = {
  lineNumbers: 'on' | 'off'
  minimap: { enabled: boolean }
}

const URL_CODE_HASH_PREFIX = '#code/'

const DEFAULT_EDITOR_OPTIONS: EditorOptions = {
  lineNumbers: 'on',
  minimap: { enabled: false },
} as const

const DEFAULT_TRANSPILER_OPTIONS: CompilerOptions = {
  target: ScriptTarget.ESNext,
  module: ModuleKind.ESNext,
  removeComments: true,
} as const

const calculateLocationHash = (tsSource: string): string => {
  const compressedSource = lz.compressToEncodedURIComponent(tsSource)

  return URL_CODE_HASH_PREFIX + compressedSource
}

type TsPlaygroundProps = Record<string, never>

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const TsPlayground: React.FC = (_props: TsPlaygroundProps) => {
  const [tsSourceCode, setTsSourceCode] = useState('')
  const [jsSourceCode, jsSourceCodeRef, setJsSourceCode] = useRefState('')
  const [tsErrors, setTsErrors] = useState<string[]>([])
  const [logs, setLogs] = useState('')
  const [editorOptions, setEditorOptions] = useState(DEFAULT_EDITOR_OPTIONS)
  const [compilerOptions, setCompilerOptions] = useState(
    DEFAULT_TRANSPILER_OPTIONS
  )
  const [sharedLinks, setSharedLinks] = useState<string[]>([])
  const [jsVisible, setJsVisible] = useState(true)
  const [linksVisible, setLinksVisible] = useState(true)

  // https://kyleshevlin.com/debounce-and-throttle-callbacks-with-react-hooks
  // https://github.com/facebook/react/issues/19240#issuecomment-652945246
  // https://codesandbox.io/s/ts-typechecking-in-the-browser-gg9il?file=/src/typecheck.ts
  const transpileTsSourceAndUpdateUrl = useMemo(
    () =>
      debounce((tsSource: string, options: CompilerOptions) => {
        transpileTs(tsSource, options).then(transpilationResult => {
          setJsSourceCode(transpilationResult.emittedFile)

          setTsErrors(
            transpilationResult.diagnostics.map(diagnostic => {
              return `[${diagnostic.type}] Row ${diagnostic.line + 1}, col ${
                diagnostic.character + 1
              }: ${diagnostic.message} (${diagnostic.code})`
            })
          )
        })

        window.location.hash = calculateLocationHash(tsSource)
      }, 500),
    [setJsSourceCode]
  )

  const handleTsEditorChange = useCallback(
    (tsSource?: string) => {
      tsSource ??= ''

      setTsSourceCode(tsSource)
      transpileTsSourceAndUpdateUrl(tsSource, compilerOptions)
    },
    [transpileTsSourceAndUpdateUrl, compilerOptions]
  )

  useEffect(() => {
    const compressedTsSource = window.location.hash.slice(
      URL_CODE_HASH_PREFIX.length
    )

    if (compressedTsSource !== '') {
      const uncompressedTsSource =
        lz.decompressFromEncodedURIComponent(compressedTsSource) ?? ''

      handleTsEditorChange(uncompressedTsSource)
    }
  }, [handleTsEditorChange])

  const executeTs = () => {
    const jsSource =
      `
      const logResults = [];

      // Save 'console.log()' function.
      const previousLogFn = console.log;

      // Override 'console.log'.
      console.log = (...args) => {
        logResults.push(args);
      };
      \n` +
      '//////////////////////////////////////////////////////\n' +
      '//               BEGIN - TRANSPILED CODE            //\n' +
      '//////////////////////////////////////////////////////\n\n' +
      jsSourceCodeRef.current +
      '\n' +
      '//////////////////////////////////////////////////////\n' +
      '//                END - TRANSPILED CODE             //\n' +
      '//////////////////////////////////////////////////////\n' +
      `
      // Restore previous 'console.log()' function.
      console.log = previousLogFn;

      return logResults;`

    let results: (string | unknown[])[]

    try {
      results = new Function(jsSource)()
    } catch (error) {
      results = [getErrorMessage(error)]
    }

    setLogs(
      previousOutput =>
        previousOutput.concat(
          '-'.repeat(20) +
            ' ' +
            new Date(Date.now()).toLocaleString('pt-BR') +
            ' ' +
            '-'.repeat(20) +
            '\n\n'
        ) +
        results
          .map((item: unknown) =>
            item instanceof Array
              ? item.map(formatLogEntry).join(' ')
              : formatLogEntry(item)
          )
          .join('\n') +
        '\n\n'
    )
  }

  const shareLink = () => {
    const url = window.location.href

    createShortUrlApi(url)
      .then(response => {
        const visitLink = response.data.links.visit

        copyTextToClipboard(visitLink)
        setSharedLinks(previousSharedLinks => [
          visitLink,
          ...previousSharedLinks,
        ])
      })
      .catch(error => {
        console.log(
          `Error while creating short link in backend: ('${error.message}')`
        )

        copyTextToClipboard(url)
        setSharedLinks(previousSharedLinks => [url, ...previousSharedLinks])
      })
  }

  const clearLogs = () => setLogs('')

  const updateEditorOption = (key: keyof EditorOptions, newValue: unknown) => {
    const newEditorOptions = { ...editorOptions, [key]: newValue }

    setEditorOptions(newEditorOptions)
  }

  const updateCompilerOption = (
    key: keyof CompilerOptions,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    newValue: any
  ) => {
    const newCompilerOptions = { ...compilerOptions, [key]: newValue }

    setCompilerOptions(newCompilerOptions)
  }

  // https://microsoft.github.io/monaco-editor/playground.html#interacting-with-the-editor-adding-an-action-to-an-editor-instance
  // https://github.com/microsoft/monaco-editor/blob/212670c/website/typedoc/monaco.d.ts
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleTsEditorMount = (editor: any, monaco: any) => {
    editor.addAction({
      id: 'execute-ts',
      label: 'Execute TypeScript',
      keybindings: [monaco.KeyMod.CtrlCmd | monaco.KeyCode.Enter],
      run: executeTs,
    })

    editor.addAction({
      id: 'clear-logs',
      label: 'Clear logs',
      keybindings: [monaco.KeyMod.CtrlCmd | monaco.KeyCode.KeyK],
      run: clearLogs,
    })

    editor.addAction({
      id: 'share-link',
      label: 'Share link',
      keybindings: [monaco.KeyMod.CtrlCmd | monaco.KeyCode.KeyS],
      run: shareLink,
    })
  }

  return (
    <div>
      <h2>TS</h2>
      <input
        type='checkbox'
        id='line-numbers'
        checked={editorOptions.lineNumbers === 'on'}
        onChange={e =>
          updateEditorOption('lineNumbers', e.target.checked ? 'on' : 'off')
        }
      />
      <label htmlFor='line-numbers'>lineNumbers</label>
      &nbsp;&nbsp;&nbsp;
      <input
        type='checkbox'
        id='minimap'
        checked={editorOptions.minimap.enabled}
        onChange={e =>
          updateEditorOption('minimap', { enabled: e.target.checked })
        }
      />
      <label htmlFor='minimap'>minimap</label>
      <Editor
        language='typescript'
        value={tsSourceCode}
        height='50vh'
        width='75vw'
        theme='vs-dark'
        onChange={handleTsEditorChange}
        options={{
          ...editorOptions,
          formatOnType: true,
        }}
        onMount={handleTsEditorMount}
      />
      {tsErrors.length > 0 && (
        <>
          <h2>Errors</h2>
          <ul>
            {tsErrors.map((error, i) => (
              <li key={i}>{error}</li>
            ))}
          </ul>
        </>
      )}
      <h2 onClick={() => setJsVisible(previousJsVisible => !previousJsVisible)}>
        JS ({jsVisible ? 'Hide' : 'Show'})
      </h2>
      {jsVisible && (
        <>
          <label>
            Target: &nbsp;
            <select
              id='compiler-select-target'
              value={compilerOptions.target}
              onChange={e =>
                updateCompilerOption('target', parseInt(e.target.value))
              }
            >
              {groupEnum(ScriptTarget).map(([target, key]) => (
                <option key={key} value={key}>
                  {target}
                </option>
              ))}
            </select>
          </label>
          &nbsp;&nbsp;&nbsp;
          <label>
            Module: &nbsp;
            <select
              id='compiler-select-module'
              value={compilerOptions.module}
              onChange={e =>
                updateCompilerOption('module', parseInt(e.target.value))
              }
            >
              {groupEnum(ModuleKind).map(([module, key]) => (
                <option key={key} value={key}>
                  {module}
                </option>
              ))}
            </select>
          </label>
          &nbsp;&nbsp;&nbsp;
          <input
            type='checkbox'
            id='remove-comments'
            checked={compilerOptions.removeComments}
            onChange={e =>
              updateCompilerOption('removeComments', e.target.checked)
            }
          />
          <label htmlFor='remove-comments'>removeComments</label>
          <Editor
            language='javascript'
            value={jsSourceCode}
            height='20vh'
            width='75vw'
            theme='vs-dark'
            options={{
              readOnly: true,
              lineNumbers: 'off',
              minimap: { enabled: false },
            }}
          />
        </>
      )}
      <br />
      <button disabled={tsSourceCode === ''} onClick={executeTs}>
        Execute (Cmd + Enter)
      </button>
      &nbsp;
      <button disabled={logs === ''} onClick={clearLogs}>
        Clear Logs (Cmd + K)
      </button>
      &nbsp;
      <button disabled={tsSourceCode === ''} onClick={shareLink}>
        Share Link (Cmd + S)
      </button>
      <h2>Logs</h2>
      <textarea rows={20} cols={100} value={logs} readOnly={true} />
      {sharedLinks.length > 0 && (
        <>
          <h2
            onClick={() =>
              setLinksVisible(previousLinksVisible => !previousLinksVisible)
            }
          >
            Links ({linksVisible ? 'Hide' : 'Show'})
          </h2>
          {linksVisible && (
            <ul>
              {sharedLinks.map((link, i) => (
                <li key={`${link}-${i}`}>
                  <a href={link} rel='noreferrer' target='_blank'>
                    {link}
                  </a>
                </li>
              ))}
            </ul>
          )}
        </>
      )}
    </div>
  )
}

export default TsPlayground
