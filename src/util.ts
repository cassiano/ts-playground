import ts, { CompilerOptions } from 'typescript'
import {
  createDefaultMapFromCDN,
  createSystem,
  createVirtualTypeScriptEnvironment,
} from '@typescript/vfs'
import { ScriptTarget } from 'typescript'

// export type GenericDiagnosticWithType = Awaited<
//   ReturnType<typeof transpileTs>
// >['diagnostics'][number]

export type DiagnosticWithType = ts.Diagnostic & {
  type: string
}

export type DiagnosticWithLocationAndType = ts.DiagnosticWithLocation & {
  type: string
}

type GenericDiagnosticWithType =
  | DiagnosticWithType
  | DiagnosticWithLocationAndType

export type CompleteGenericDiagnostic = GenericDiagnosticWithType & {
  line: number
  character: number
  message: string
}

// https://www.typescriptlang.org/dev/typescript-vfs/
// https://snyk.io/advisor/npm-package/typescript/functions/typescript.createProgram
// https://jakerunzer.com/running-ts-in-browser
// https://www.npmjs.com/package/@typescript/vfs/v/1.1.2
export const transpileTs = async (
  tsSource: string,
  transpilerOptions: CompilerOptions
) => {
  const DUMMY_FILENAME = 'index.ts'

  // Notice that it won't work for 'ES2022', due to the lack of corresponding values in
  // version 1.4.0 of file https://www.npmjs.com/package/@typescript/vfs?activeTab=explore
  // Check the `files` variable/constant in line 108.
  //
  // For 'ESNext', the problem is that ScriptTarget.ESNext and ScriptTarget.Latest have the same
  // value, so ScriptTarget[ScriptTarget.ESNext] in fact returns the string "Latest" instead of
  // "ESNext". Check the expression in line 109 of the same file (see above).
  const lib = ['ES2021']
  if (transpilerOptions.target === ScriptTarget.ESNext) lib.push('ESNext')

  const fsMap = await createDefaultMapFromCDN(
    {
      ...transpilerOptions,
      lib,
    },
    ts.version,
    true,
    ts
  )
  fsMap.set(DUMMY_FILENAME, tsSource)

  const system = createSystem(fsMap)

  const env = createVirtualTypeScriptEnvironment(system, [DUMMY_FILENAME], ts, {
    ...transpilerOptions,
    lib: ['DOM', 'ES2021'],
  })

  const emittedFile =
    env.languageService.getEmitOutput(DUMMY_FILENAME).outputFiles?.[0].text

  const diagnostics: CompleteGenericDiagnostic[] = prepareDiagnostics(
    env.languageService
      .getSemanticDiagnostics(DUMMY_FILENAME)
      .map(
        diagnostic =>
          ({ type: 'Semantic', ...diagnostic } as DiagnosticWithType)
      )
      .concat(
        env.languageService.getSyntacticDiagnostics(DUMMY_FILENAME).map(
          diagnostic =>
            ({
              ...diagnostic,
              type: 'Syntactic',
            } as DiagnosticWithLocationAndType)
        )
      )
  )

  return {
    emittedFile,
    diagnostics,
  }
}

const prepareDiagnostics = (diagnostics: GenericDiagnosticWithType[]) => {
  return (
    diagnostics
      .map<CompleteGenericDiagnostic | undefined>(diagnostic => {
        if (!diagnostic.file || !diagnostic.start) return

        const { line, character } =
          diagnostic.file.getLineAndCharacterOfPosition(diagnostic.start)

        const message = ts.flattenDiagnosticMessageText(
          diagnostic.messageText,
          '\n'
        )

        return {
          ...diagnostic,
          line,
          character,
          message,
        }
      })
      .filter(
        diagnostic => diagnostic !== undefined
      ) as CompleteGenericDiagnostic[]
  ).sort((diagnosticA, diagnosticB) => {
    return diagnosticA.line !== diagnosticB.line
      ? diagnosticA.line - diagnosticB.line
      : diagnosticA.character - diagnosticB.character
  })
}

export const copyTextToClipboard = (text: string): void => {
  if (navigator?.clipboard?.writeText) {
    navigator.clipboard
      .writeText(text)
      .then(() => console.log(`Text '${text}' copied to clipboard.`))
  }
}

export const debounce = (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  fn: (...args: any[]) => void,
  timeout: number
): ((...args: unknown[]) => void) => {
  let timerId: ReturnType<typeof setTimeout> | null = null

  return (...args: unknown[]): void => {
    if (timerId !== null) clearTimeout(timerId)

    timerId = setTimeout(() => {
      fn(...args)

      timerId = null
    }, timeout)
  }
}

export const throttle = (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  fn: (...args: any[]) => void,
  timeout: number,
  eagerExecution = false
): ((...args: unknown[]) => void) => {
  let timerId: ReturnType<typeof setTimeout> | null = null

  return (...args: unknown[]): void => {
    if (timerId !== null) return // Return immediately if timer still pending.

    if (eagerExecution) fn(...args)

    timerId = setTimeout(() => {
      if (!eagerExecution) fn(...args)

      timerId = null
    }, timeout)
  }
}

// https://kentcdodds.com/blog/get-a-catch-block-error-message-with-typescript
export const getErrorMessage = (error: unknown) => {
  return error instanceof Error ? error.message : String(error)
}

export const safelyConvertToJson = (
  value: object,
  replacerFn?: (...args: unknown[]) => void,
  spacing?: number
) => {
  try {
    return JSON.stringify(value, replacerFn, spacing)
  } catch (error) {
    return getErrorMessage(error)
  }
}

export const formatLogEntry = (entry: unknown) => {
  if (entry instanceof Array) {
    return safelyConvertToJson(entry)
  } else if (entry instanceof Object) {
    return safelyConvertToJson(entry, undefined, 2)
  } else {
    return entry
  }
}

// Transforms { '1': 'A', '2': 'B', 'A': 1, 'B': 2, 'C': 1 } into { 'A | C': 1, B: 2 }.
export const groupEnum = (enumerator: object, delimiter = ' | ') => {
  // Transforms { '1': 'A', '2': 'B', 'A': 1, 'B': 2, 'C': 1 } into
  // { '1': 'A', '2': 'B', 'A | C': 1, 'B': 2 }.
  const groupLabelsByValue = (keyValuePairs: [string, number][]) =>
    Object.entries(
      keyValuePairs.reduce<Record<number, string>>(
        (memo, [label, value]) => ({
          ...memo,
          [value]:
            memo[value] === undefined
              ? label
              : [label, memo[value]].join(delimiter),
        }),
        {}
      )
    ).reduce<Record<string, number>>((memo, [value, label]) => {
      memo[label] = parseInt(value)
      return memo
    }, {})

  return Object.entries(
    groupLabelsByValue(
      Object.entries(enumerator).filter(
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        ([_module, key]) => !isNaN(Number(key))
      ) as [string, number][]
    )
  )
}
