import React from 'react'
import './App.css'
import { TsPlayground } from './components/ts-playground'

type AppProps = Record<string, never>

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const App: React.FC<AppProps> = (_props: AppProps) => {
  return <TsPlayground />
}

export default App
