import axios from 'axios'
import { ShortUrlType } from '../components/ts-playground'

axios.defaults.baseURL = 'http://localhost:3001/api'

export const createShortUrlApi = (url: string) =>
  axios.post<ShortUrlType>('/short_urls', { url })
